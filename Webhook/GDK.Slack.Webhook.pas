unit GDK.Slack.Webhook;

interface

uses
  GDK.Slack.Message.Interfaces, GDK.Slack.Webhook.Interfaces, IdHTTP, IdSSLOpenSSL;

type
  TSlackWebHook= class(TInterfacedObject, ISlackWebHook)
  private
    FWebhook: string;
    HTTPClient: TIdHTTP;
    SSLHandler: TIdSSLIOHandlerSocketOpenSSL;
  public
    constructor Create(const AWebhook: string);
    destructor Destroy; override;
    procedure PostMessage(ASlackMessage: ISlackMessage);
  end;

implementation

uses
  System.Classes, System.SysUtils;

{ ITSlackWebHook }

constructor TSlackWebHook.Create(const AWebhook: string);
begin
  FWebhook := AWebhook;
  HTTPClient := TIdHTTP.Create;

  SSLHandler := TIdSSLIOHandlerSocketOpenSSL.Create(nil);
  HTTPClient.IOHandler := SSLHandler;
end;

destructor TSlackWebHook.Destroy;
begin
  SSLHandler.Free;
  HTTPClient.Free;
  inherited;
end;

procedure TSlackWebHook.PostMessage(ASlackMessage: ISlackMessage);
var
  Payload: TStringStream;
begin
  Payload := TStringStream.Create(ASlackMessage.AsSlackMessage, TEncoding.UTF8);
  try
    HTTPClient.Post(FWebhook, Payload);
  finally
    Payload.Free;
  end;
end;

end.
