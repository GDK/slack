unit GDK.Slack.Webhook.Interfaces;

interface

uses
  GDK.Slack.Message.Interfaces;

type
  ISlackWebHook = interface(IInterface)
    ['{06D1689A-8F8E-4F6F-9F7A-72A89D30A76B}']

    procedure PostMessage(ASlackMessage: ISlackMessage);
  end;

implementation

end.
