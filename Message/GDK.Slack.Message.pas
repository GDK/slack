unit GDK.Slack.Message;

interface

uses
  GDK.Slack.Message.Interfaces;

type
  TSlackMessage = class(TInterfacedObject, ISlackMessage)
  private
    FText: string;
    FBodyText: string;
    FTitle: string;
    FUrl: string;
    FUsername: string;
    FAuthorName: string;
    FIsDanger: Boolean;

    function EscapeMessage(const AMessage: string): string;
  public
    function Title(const ATitle: string): ISlackMessage;
    function URL(const AURL: string): ISlackMessage;
    function BodyText(const ABodyText: string): ISlackMessage;
    function Text(const AText: string): ISlackMessage;
    function Username(const AUsername: string): ISlackMessage;
    function AuthorName(const Value: string): ISlackMessage;
    function IsDanger(const Value: Boolean): ISlackMessage;

    function AsSlackMessage: string;
  end;

implementation

uses
  System.JSON, System.Classes, System.SysUtils, System.DateUtils;

{ TSlackMessage }

function TSlackMessage.AsSlackMessage: string;
var
  SlackMessage: TJSONObject;
  SlackAttachments: TJSONArray;
  SlackAttachmentsPairs: TJSONObject;
begin
  SlackMessage:= TJSONObject.Create;
  try
    SlackMessage.AddPair(TJSONPair.Create('text', FText));

    if not FTitle.IsEmpty or
       not FUrl.IsEmpty or
       not FBodyText.IsEmpty then
    begin
      SlackAttachments := TJSONArray.Create;
      SlackAttachmentsPairs := TJSONObject.Create;
      SlackAttachmentsPairs.AddPair('title', FTitle);
      SlackAttachmentsPairs.AddPair('title_link', FUrl);
      SlackAttachmentsPairs.AddPair('text', FBodyText);

      if not FAuthorName.IsEmpty then
        SlackAttachmentsPairs.AddPair('author_name', FAuthorName);

      SlackAttachmentsPairs.AddPair('footer', 'GDK Slack Integration');
      SlackAttachmentsPairs.AddPair('ts', DateTimeToUnix(Now).ToString);
      if FIsDanger then
        SlackAttachmentsPairs.AddPair('color', 'danger');


      SlackAttachments.Add(SlackAttachmentsPairs);

      SlackMessage.AddPair('username', FUsername);
      SlackMessage.AddPair('attachments', SlackAttachments)
    end;


    Result := SlackMessage.ToJSON;

    Result := EscapeMessage(Result);
  finally
    SlackMessage.Free;
  end;
end;

function TSlackMessage.AuthorName(const Value: string): ISlackMessage;
begin
  FAuthorName := Value;
  Result := Self;
end;

function TSlackMessage.BodyText(const ABodyText: string): ISlackMessage;
begin
  FBodyText := ABodyText;
  Result := Self;
end;

function TSlackMessage.EscapeMessage(const AMessage: string): string;
begin
  // See https://api.slack.com/docs/formatting
  Result := StringReplace(AMessage, '&', '&amp', [rfReplaceAll, rfIgnoreCase]);
  Result := StringReplace(Result, '<', '&lt;', [rfReplaceAll, rfIgnoreCase]);
  Result := StringReplace(Result, '>', '&gt;', [rfReplaceAll, rfIgnoreCase]);
end;

function TSlackMessage.IsDanger(const Value: Boolean): ISlackMessage;
begin
  FIsDanger := Value;
  Result := Self;
end;

function TSlackMessage.Text(const AText: string): ISlackMessage;
begin
  FText := AText;
  Result := Self;
end;

function TSlackMessage.Title(const ATitle: string): ISlackMessage;
begin
  FTitle := ATitle;
  Result := Self;
end;

function TSlackMessage.URL(const AURL: string): ISlackMessage;
begin
  FUrl := AUrl;
  Result := Self;
end;

function TSlackMessage.Username(const AUsername: string): ISlackMessage;
begin
  FUsername := AUsername;
  Result := Self;
end;

end.
