unit GDK.Slack.Message.Interfaces;

interface

type
  ISlackMessage = interface(IInterface)
    ['{5D4B630A-CF84-4979-92AF-D3140D912281}']
    function Title(const ATitle: string): ISlackMessage;
    function URL(const AURL: string): ISlackMessage;
    function BodyText(const ABodyText: string): ISlackMessage;
    function Text(const AText: string): ISlackMessage;
    function Username(const AText: string): ISlackMessage;
    function AuthorName(const Value: string): ISlackMessage;
    function IsDanger(const Value: Boolean): ISlackMessage;

    function AsSlackMessage: string;
  end;

implementation

end.
