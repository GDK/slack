unit Test.Slack.Webhook;

interface

uses
  DUnitX.TestFramework;

type
  [TestFixture]
  TSlackConnectTest = class(TObject)
  public
    [Test]
    [TestCase('TestSimpleWebhook','This is a simple Webhook test')]
    [TestCase('TestSimpleWebhookWithURL','This is a simple Webhook with an URL: http://google.nl')]
    procedure TestSimpleWebhook(const AText: string);

    [Test]
    [TestCase('TestComplexWebhookWithTitle','This is a complex message, Link to Google,Body,http://google.nl')]
    [TestCase('TestComplexWebhookWithTitle','This is a complex message with an URL: http://google.nl,Link to Google,Body,http://google.nl')]
    procedure TestComplexWebhook(const AText, ATitle, ABody, AUrl: string);

  end;

implementation

uses
  GDK.Slack.Webhook.Interfaces, GDK.Slack.Webhook, GDK.Slack.Message, GDK.Slack.Message.Interfaces;

procedure TSlackConnectTest.TestComplexWebhook(const AText, ATitle, ABody,
  AUrl: string);
var
  SlackWebhook: ISlackWebhook;
  ExpectedJSon: string;
  SlackMessage: ISlackMessage;
begin
  SlackWebhook := TSlackWebhook.Create('https://hooks.slack.com/services/<tag>');
  SlackMessage := TSlackMessage.Create;

  SlackWebhook.PostMessage(
    SlackMessage
      .Title(ATitle)
      .BodyText(ABody)
      .URL(AUrl)
      .Text(AText));

end;

procedure TSlackConnectTest.TestSimpleWebhook(const AText: string);
var
  SlackWebhook: ISlackWebhook;
  ExpectedJSon: string;
  SlackMessage: ISlackMessage;
begin
  SlackWebhook := TSlackWebhook.Create('https://hooks.slack.com/services/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');
  SlackMessage := TSlackMessage.Create;

  SlackWebhook.PostMessage(
    SlackMessage
      .Text(AText));

  // Todo Add assert
end;

initialization
  TDUnitX.RegisterTestFixture(TSlackConnectTest);
end.

