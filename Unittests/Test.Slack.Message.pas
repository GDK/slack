unit Test.Slack.Message;

interface

uses
  DUnitX.TestFramework;

type
  [TestFixture]
  TSlackMessageTest = class(TObject)
  private
    function EscapeURL(const AURL: string): string;
  public
    [Test]
    [TestCase('TestSimpleMessage','This is a simple message')]
    [TestCase('TestSimpleMessageWithURL','This is a simple message with an URL: http://google.nl')]
    procedure TestSimpleMessage(const AText: string);

    [Test]
    [TestCase('TestComplexMessage','This is a simple message, TestTitle,Body,http://google.nl')]
    [TestCase('TestComplexMessageWithURL','This is a simple message with an URL: http://google.nl,Title,Body,http://google.nl')]
    procedure TestComplexMessage(const AText, ATitle, ABody, AUrl: string);

  end;

implementation

uses
  GDK.Slack.Message.Interfaces, GDK.Slack.Message, System.SysUtils;

function TSlackMessageTest.EscapeURL(const AURL: string): string;
begin
  Result := StringReplace(AURL, '/', '\/', [rfReplaceAll]);
end;

procedure TSlackMessageTest.TestComplexMessage(const AText, ATitle, ABody, AUrl: string);
var
  SlackMessage: ISlackMessage;
  SlackJson: string;
  ExpectedJSon: string;
begin
  SlackMessage := TSlackMessage.Create;
  SlackJson := SlackMessage
                  .Text(AText)
                  .Title(ATitle)
                  .BodyText(ABody)
                  .URL(AUrl)
                  .AsSlackMessage;

  ExpectedJSon := '{"text":"'+EscapeURL(AText)+'","attachments":[{'+
            '"title":"'+ATitle+'",'+
            '"title_link":"'+EscapeURL(AURL)+'",'+
            '"text":"'+ABody+'"}]}';

  Assert.AreEqual(ExpectedJSon, SlackJson);
end;

procedure TSlackMessageTest.TestSimpleMessage(const AText: string);
var
  SlackMessage: ISlackMessage;
  SlackJson: string;
  ExpectedJSon: string;
begin
  SlackMessage := TSlackMessage.Create;
  SlackJson := SlackMessage.Text(AText).AsSlackMessage;

  ExpectedJSon := '{"text":"'+EscapeURL(AText)+'"}';

  Assert.AreEqual(ExpectedJSon, SlackJson);

end;

initialization
  TDUnitX.RegisterTestFixture(TSlackMessageTest);
end.
